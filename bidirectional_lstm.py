import os
import tensorflow as tf
from tensorflow.contrib import rnn
from tensorflow.examples.tutorials.mnist import input_data

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

dataset = mnist

learning_rate = 0.001
learning_iters = 10000
display_step = 200
conv_loss = 0.001
conv_acc = 0.999
num_units = 128
# num_layers = 2
# num_bid_layers = 2
input_dim = 28
output_dim = 10
time_steps = 28
batch_size = 128

X = tf.placeholder(tf.float32, [None, time_steps, input_dim])
Y = tf.placeholder(tf.float32, [None, output_dim])

weights = tf.Variable(tf.random_normal([2 * num_units, output_dim]))
biases = tf.Variable(tf.random_normal([output_dim]))

X_seq = tf.unstack(X, time_steps, 1)

# multi-layer-bidirectinal with one-layer-lstm
# lstm_cells_fw = [rnn.BasicLSTMCell(num_units) for _ in range(num_layers)]
# lstm_cells_bw = [rnn.BasicLSTMCell(num_units) for _ in range(num_layers)]
# outputs, state_fw, state_bw = rnn.stack_bidirectional_rnn(lstm_cells_fw, lstm_cells_bw, X_seq, dtype=tf.float32)

# multi-layer-bidirectinal with multi-layer-lstm
# lstm_cells_fw = [rnn.MultiRNNCell([rnn.BasicLSTMCell(num_units) for _ in range(num_layers)]) for _ in range(num_bid_layers)]
# lstm_cells_bw = [rnn.MultiRNNCell([rnn.BasicLSTMCell(num_units) for _ in range(num_layers)]) for _ in range(num_bid_layers)]
# outputs, state_fw, state_bw = rnn.stack_bidirectional_rnn(lstm_cells_fw, lstm_cells_bw, X_seq, dtype=tf.float32)

# one-layer-bidirectinal with multi-layer-lstm
# lstm_cells_fw = rnn.MultiRNNCell([rnn.BasicLSTMCell(num_units) for _ in range(num_layers)])
# lstm_cells_bw = rnn.MultiRNNCell([rnn.BasicLSTMCell(num_units) for _ in range(num_layers)])
# outputs, state_fw, state_bw = rnn.static_bidirectional_rnn(lstm_cells_fw, lstm_cells_bw, X_seq, dtype=tf.float32)

lstm_cell_fw = rnn.BasicLSTMCell(num_units)
lstm_cell_bw = rnn.BasicLSTMCell(num_units)
outputs, state_fw, state_bw = rnn.static_bidirectional_rnn(lstm_cell_fw, lstm_cell_bw, X_seq, dtype=tf.float32)

logits = tf.matmul(outputs[-1], weights) + biases
pred = tf.nn.softmax(logits)

loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=Y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(loss_op)

correct_pred = tf.equal(tf.argmax(pred, 1), tf.argmax(Y, 1))
acc_op = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

init_op = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init_op)

    print('optimization starts')
    for step in range(1, learning_iters + 1):
        batch_x, batch_y = dataset.train.next_batch(batch_size)
        batch_x = batch_x.reshape(batch_size, time_steps, input_dim)

        sess.run(train_op, feed_dict={X: batch_x, Y: batch_y})
        if step % display_step == 0 or step == 1:
            loss, acc = sess.run([loss_op, acc_op], feed_dict={X: batch_x, Y: batch_y})
            print('step={:<5d}\tloss={:.10g}\tacc={:.10g}'.format(step, loss, acc))
            if loss < conv_loss and acc > conv_acc:
                break
    print('optimization finished')

    test_len = 128
    test_data = dataset.test.images[:test_len].reshape([-1, time_steps, input_dim])
    test_label = dataset.test.labels[:test_len]
    acc = sess.run(acc_op, feed_dict={X: test_data, Y: test_label})
    print('test acc={}'.format(acc))
    prediction_demo = sess.run(pred, feed_dict={X: test_data[-3:], Y: test_label[-3:]})
    print(prediction_demo)
    print(sess.run(tf.argmax(prediction_demo, axis=1)))
