import tensorflow as tf
from tensorflow.contrib import rnn

# demo data
train_X = [  # batch = 4
    [  # time_steps = 3
        [0, 0, 0, 0],  # inputs_dim = 4
        [0, 0, 0, 1],
        [0, 0, 1, 0]
    ],
    [
        [0, 0, 1, 1],
        [0, 1, 0, 0],
        [0, 1, 0, 1]
    ],
    [
        [0, 1, 1, 0],
        [0, 1, 1, 1],
        [1, 0, 0, 0]
    ],
    [
        [1, 0, 0, 1],
        [1, 0, 1, 0],
        [1, 0, 1, 1]
    ]
]
train_Y = [
    [[0, 0, 0, 1], [0, 0, 1, 0], [0, 0, 1, 1]],
    [[0, 1, 0, 0], [0, 1, 0, 1], [0, 1, 1, 0]],
    [[0, 1, 1, 1], [1, 0, 0, 0], [1, 0, 0, 1]],
    [[1, 0, 1, 0], [1, 0, 1, 1], [1, 1, 0, 0]]
]
train_L = [3, 3, 3, 3]

learning_rate = 0.001
learning_iters = 1000
display_step = 100
conv_loss = 0.001
conv_acc = 0.999
num_units = 128
input_dim = 4
output_dim = 4
time_steps = 3
batch_size = 4

x = tf.placeholder(tf.float32, [None, time_steps, input_dim])
y = tf.placeholder(tf.float32, [None, time_steps, output_dim])
seqlen = tf.placeholder(tf.int32, [None])

# 2 * num_units for both forward and backward cells
weights = tf.Variable(tf.random_normal([2 * num_units, output_dim]))
biases = tf.Variable(tf.random_normal([output_dim]))

lstm_cell_fw = rnn.BasicLSTMCell(num_units)
lstm_cell_bw = rnn.BasicLSTMCell(num_units)
# inputs should be shape of [batch_size, time_steps, input_dim]
outputs, states = tf.nn.bidirectional_dynamic_rnn(lstm_cell_fw,
                                                  lstm_cell_bw,
                                                  x,
                                                  dtype=tf.float32,
                                                  sequence_length=seqlen)
# concat forward and backward outputs to shape of [2 x output_dim]
outputs = tf.concat(outputs, 2)
outputs = tf.reshape(outputs, [-1, 2 * num_units])
logits = tf.matmul(outputs, weights) + biases

loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits,
                                                                 labels=y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(loss_op)

pred = tf.nn.softmax(logits)
pred = tf.reshape(pred, [-1, time_steps, output_dim])
correct_pred = tf.equal(tf.argmax(pred, 2), tf.argmax(y, 2))
acc_op = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

init_op = tf.global_variables_initializer()

sess = tf.Session()
sess.run(init_op)

print('optimization starts')

for step in range(1, learning_iters + 1):
    batch_x, batch_y, batch_l = train_X, train_Y, train_L

    sess.run(train_op, feed_dict={x: batch_x, y: batch_y, seqlen: batch_l})

    if step % display_step == 0 or step == 1:
        loss, acc = sess.run([loss_op, acc_op], feed_dict={
            x: batch_x,
            y: batch_y,
            seqlen: batch_l
        })
        print('step={:<5d}\tloss={:.10g}\tacc={:.10g}'.format(step, loss, acc))

print('optimization finished')

# predict demo
prediction_demo = sess.run(pred, feed_dict={
    x: train_X[:1],
    y: train_Y[:1],
    seqlen: train_L[:1]
})
print(prediction_demo)

sess.close()
