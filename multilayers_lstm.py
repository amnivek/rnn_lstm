import os
import tensorflow as tf
from tensorflow.contrib import rnn
from tensorflow.examples.tutorials.mnist import input_data

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

dataset = mnist

learning_rate = 0.001
learning_iters = 10000
display_step = 200
conv_loss = 0.001
conv_acc = 0.999
num_units = 128
num_layers = 2
input_dim = 28
output_dim = 10
time_steps = 28
batch_size = 128

X = tf.placeholder(tf.float32, [None, time_steps, input_dim])
Y = tf.placeholder(tf.float32, [None, output_dim])

weights = tf.Variable(tf.random_normal([num_units, output_dim]))
biases = tf.Variable(tf.random_normal([output_dim]))

# Prepare data shape to match `rnn` function requirements
# Current data input shape: (batch_size, time_steps, input_dim)
# Required shape: 'time_steps' tensors list of shape (batch_size, input_dim)
# Unstack to get a list of 'time_steps' tensors of shape (batch_size, input_dim)
X_seq = tf.unstack(X, time_steps, 1)

lstm_cells = [rnn.BasicLSTMCell(num_units) for _ in range(num_layers)]
stacked_lstm = rnn.MultiRNNCell(lstm_cells)
outputs, states = rnn.static_rnn(stacked_lstm, X_seq, dtype=tf.float32)

logits = tf.matmul(outputs[-1], weights) + biases
pred = tf.nn.softmax(logits)

loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=Y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(loss_op)

correct_pred = tf.equal(tf.argmax(pred, 1), tf.argmax(Y, 1))
acc_op = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

init_op = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init_op)

    # train
    print('optimization starts')
    for step in range(1, learning_iters + 1):
        batch_x, batch_y = dataset.train.next_batch(batch_size)
        batch_x = batch_x.reshape(batch_size, time_steps, input_dim)

        sess.run(train_op, feed_dict={X: batch_x, Y: batch_y})
        if step % display_step == 0 or step == 1:
            loss, acc = sess.run([loss_op, acc_op], feed_dict={X: batch_x, Y: batch_y})
            print('step={:<5d}\tloss={:.10g}\tacc={:.10g}'.format(step, loss, acc))
            if loss < conv_loss and acc > conv_acc:
                break
    print('optimization finished')

    # test
    test_len = 128
    test_data = dataset.test.images[:test_len].reshape([-1, time_steps, input_dim])
    test_label = dataset.test.labels[:test_len]
    acc = sess.run(acc_op, feed_dict={X: test_data, Y: test_label})
    print('test acc={}'.format(acc))
